import {Component} from "react";

class FragmentLifecycle extends Component {
	componentDidMount() {
		this.props.didMount();
	}

	render() {
		return this.props.children;
	}
}

export default FragmentLifecycle;