import React from "react";

import classnames from 'classnames';

import styles from "./index.module.scss";

const Space = props => {
	let {height, variant, className, ...rest} = props;
	let style = height ? {height: height} : {};

	return (
		<div style={style} variant={variant} className={classnames(className, styles.root)} {...rest} />
	);
}

export default Space;