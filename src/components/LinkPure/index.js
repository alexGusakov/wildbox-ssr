import React, {memo} from "react";
//import { Link } from "react-router-dom";


// <Link to={to} {...rest} style={Object.assign({textDecoration: "none", color: "inherit"}, style)}>{children}</Link>

const LinkPure = props => {
	let {to, children, style = {}, ...rest} = props;
	return (

		<a href={to} {...rest} style={Object.assign({textDecoration: "none", color: "inherit"}, style)}>{children}</a>
	);
}

export default memo(LinkPure);