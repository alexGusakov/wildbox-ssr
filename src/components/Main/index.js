import React from "react";
import Helmet from "react-helmet";
import { withRouter } from "react-router-dom";
import classnames from "classnames";
import { Container, Row, Col } from "react-grid-system";
import { Table } from "antd";
import { Divider } from "antd";
import { AutoComplete } from "antd";
import { Select } from "antd";

import FragmentLifecycle from "../FragmentLifecycle"

import classes from "../App/index.module.scss";
import mixins from "../../css/mixins.module.scss";

const { Option } = Select


const Main = props => {
	const { handlers, state, data } = props

	return (
		<FragmentLifecycle didMount={handlers.resetState}>
			{/*<Helmet>
				<title>Wildbox</title>
				<meta name="description" content="Бесплатная аналитика брендов Wildberries" />
			</Helmet>*/}

			<Container>
				<div className={classes.tableWrapper}>
					<div className={classes.tableTitle}>Бесплатная аналитика брендов Wildberries</div>
					<Divider style={{margin: "20px 0"}} />
					<Row>
						<Col xs={12} sm={12} md={8} className={classes.inputCol}>
							<AutoComplete
								size="large"
								allowClear
								placeholder="Поиск по названию бренда"
								prefix={
									<img alt="" src={require("../../img/other/search.svg")}
										className={classes.inputIcon}
									/>
								}
								className={mixins.w100}
								onSearch={handlers.onSearchBrandsDebounce}
								onSelect={value => {
									props.history.push(`/brand/${value}`)
								}}
								options={state.foundBrands}
							/>
						</Col>
						<Col xs={12} sm={12} md={4}>
							<Select
								size="large"
								value={state.mainTableDays}
								onChange={handlers.mainTableDaysOnChange}
								dropdownMatchSelectWidth={false}
								style={{width: "100%"}}
							>
								<Option value={7}>За последние 7 дней</Option>
								<Option value={14}>За последние 14 дней</Option>
								<Option value={30}>За последние 30 дней</Option>
							</Select>
						</Col>
					</Row>
					<Divider style={{margin: "20px 0"}} />
					<Table
						scroll={{x:"100%"}}
						columns={data.mainTableColumns}
						dataSource={state.mainTableData}
						loading={state.mainTableIsLoading}
						pagination={{
							onChange: (page, size) => {
								handlers.getBrandInfo({
									days: state.mainTableDays,
									page: page,
								})
							},
							//showTotal: total => `Всего: ${total}`,
							total: state.mainTableTotal,
							current: state.mainTablePage,
							pageSize: state.mainTablePerPage,
							showSizeChanger: false,
							size: state.isMobile ? "small" : "default"
						}}
						locale={{
							sortTitle: 'Сортировать',
							expand: 'Развернуть',
							collapse: 'Свернуть',
							triggerDesc: 'Нажмите для сортировки по убыванию',
							triggerAsc: 'Нажмите для сортировки по возрастанию',
							cancelSort: 'Нажмите для отмены сортировки',
						}}
					/>
				</div>
			</Container>

			<Container>
				<h1 className={classnames(classes.title, classes.titleFreeAnal)}>Бесплатная аналитика брендов Wildberries всегда под рукой</h1>
				<h5 className={classes.analSubheader}>Просто установите наш телеграм-бот и следите за любыми конкурентами!</h5>
			
				<Row className={classes.advantagesWrapper}>
					{data.advantages.map(item => (
						<Col key={item.title} className={classes.advantage}>
							<div className={classes.advantageIconWrapper}>
								<img alt="" src={item.icon} />
							</div>
							<div className={classes.advantageTitle}>{item.title}</div>
						</Col>
					))}
				</Row>
			</Container>
			
			<Container>
				<Row>
					<Col>
						<div className={classes.botWrapper}>
							<div className={classes.botLeft}>
								<img className={classes.iphone} alt="iphone screenshot" src={require("../../img/other/iphone.png")} />
								<img className={classes.iphoneShadow} alt="" src={require("../../img/other/shadow.png")} />
							</div>
							<div className={classes.botRight}>
								<div className={classes.title}>Следите за конкурентами в нашем telegram-боте</div>
								<div className={classes.description}>Наш онлайн-сервис синхронизируются с телеграм-ботом, телефоном и почтовым сервисом. Данные собираются ежедневно. Вы можете посмотреть статистику по любому бренду за любой период до 90 дней. Это удобно и бесплатно.</div>
								<button className={classes.tgButton}>
									<div>Установить telegram-бот</div>
									<img alt="" src={require("../../img/other/telegram.svg")} />
								</button>
								<img alt="" className={classes.arrow} src={require("../../img/other/arrow.svg")} />
							</div>
						</div>
					</Col>
				</Row>
			</Container>
		</FragmentLifecycle>
	)
}
export default Main;
//export default withRouter(Main);