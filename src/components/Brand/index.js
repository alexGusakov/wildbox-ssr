import React from "react";
import Helmet from "react-helmet";
import { withRouter } from "react-router-dom";
import classnames from "classnames";
import { Container, Row, Col } from "react-grid-system";
import { Table } from "antd";
import { Divider } from "antd";
import { Select } from "antd";
import { Spin } from "antd";
import { Breadcrumb } from "antd";
import { Rate } from "antd";
import {XAxis, YAxis, Tooltip, ResponsiveContainer, CartesianGrid, Line, LineChart } from "recharts";

import Space from "../Space"
import LinkPure from "../LinkPure"
import FragmentLifecycle from "../FragmentLifecycle"

import classes from "../App/index.module.scss";
import mixins from "../../css/mixins.module.scss";

const { Option } = Select

const Brand = props => {
	const { handlers, state, data } = props

	let brandAlias = props.match.params.brandAlias
	let brand = state.mainTableRawData.find(item => item.alias === brandAlias) || state.currentBrand

	return (brand ?
		<FragmentLifecycle
			didMount={() => {
				handlers.setCachedGraphData(brand)
				handlers.getCategories({
					alias: brandAlias,
					days: 7
				})
				handlers.getProductsInfo({
					alias: brandAlias,
					days: 7,
					page: 1,
					perPage: state.productsTablePerPage
				})
			}}
			key={brand.id}
		>
			{/*<Helmet>
				<title>{brand.name} на Wildberries</title>
				<meta name="description" content={`Бесплатная аналитика бренда «${brand.name}» на Wildberries`} />
			</Helmet>*/}
			<Container>
				<Breadcrumb className={classes.breadcrumbs}>
					<Breadcrumb.Item className={classes.base}><LinkPure to="/">Каталог</LinkPure></Breadcrumb.Item>
					<Breadcrumb.Item className={classes.brand}>{brand.name}</Breadcrumb.Item>
				</Breadcrumb>

				<Space variant="normal" />
			
				<div className={classnames(classes.title, mixins.mb5)}>Аналитика, продажи бренда {brand.name} на Wildberries</div>
				<h5 className={classes.subtitle}>Узнайте все о продажах {brand.name} на Wildberries</h5>

				<Space variant="small" />

				<div className={classes.brandShortInfo}>
					<div className={classes.brandLogo}>
						<img alt="brand logo" src={brand.image} />
					</div>
					<div className={classes.brandInfoBlock}>
						<div className={classes.brandInfoTitle}>
							{brand.in_categories.toLocaleString()}
						</div>
						<div className={classes.brandInfoLabel}>
							{handlers.pluralize(brand.in_categories, "Категория", "Категории", "Категорий")} товаров
						</div>
					</div>
					<div className={classes.brandInfoBlock}>
						<div className={classes.brandInfoTitle}>
							{brand.products.toLocaleString()}
						</div>
						<div className={classes.brandInfoLabel}>
							{handlers.pluralize(brand.products, "Артикул", "Артикула", "Артикулов")} на WB
						</div>
					</div>
					<div className={classes.brandInfoBlock}>
						<div className={classes.brandInfoTitle}>
							{brand.likes.toLocaleString()}
						</div>
						<div className={classes.brandInfoLabel}>
							Лайкнули этот бренд
						</div>
					</div>
					<div className={classes.brandInfoBlock}>
						<div className={classes.brandInfoTitle}>
							<Rate className={classes.rate} allowHalf defaultValue={brand.rating} disabled />
						</div>
						<div className={classes.brandInfoLabel}>
							Средняя оценка товаров
						</div>
					</div>
					<div className={classes.brandInfoBlock}>
						<div className={classes.brandInfoTitle}>
							{brand.reviews.toLocaleString()}
						</div>
						<div className={classes.brandInfoLabel}>
							{handlers.pluralize(brand.reviews, "Отзыв", "Отзыва", "Отзывов")}
						</div>
					</div>
				</div>

				<div className={classnames(classes.tableWrapper, mixins.mb10)}>
					<div className={classes.tableTitle}>История изменения продаж {brand.name}</div>
					<Divider style={{margin: "20px 0"}} />
					<Row>
						<Col xs={12} sm={12} md={12}>
							<Select size="large" value={state.salesGraphDays} onChange={value => handlers.salesGraphDaysOnChange(value, brandAlias)} dropdownMatchSelectWidth={false} style={{width: "100%"}}>
								<Option value={7}>За последние 7 дней</Option>
								<Option value={14}>За последние 14 дней</Option>
								<Option value={30}>За последние 30 дней</Option>
							</Select>
						</Col>
					</Row>
					<Divider style={{margin: "20px 0"}} />
					<ResponsiveContainer width="100%" height={460} className={state.salesGraphIsLoading ? mixins.graphDisabled : null}>
						<LineChart
							data={state.salesGraphData}
							margin={{ top: 10, right: 10, bottom: 0, left: 0 }}
						>
							<CartesianGrid vertical={false} strokeDasharray="3 3" />
							<XAxis axisLine={false} tickLine={false} dataKey="name"  />
							<YAxis tickFormatter={str => Number(str).toLocaleString()} width={70} axisLine={false} tickLine={false} />
							<Tooltip separator={": "} formatter={(value, name) => [value.toLocaleString(), "Количество"]} />
							<Line activeDot={{ fill: "white", stroke: "#F23E5F", strokeWidth: 3, r: 7 }} animationDuration={900} type="monotone" dataKey="value" strokeWidth={3} stroke="#F23E5F" fillOpacity={1} fill="#F23E5F" />
						</LineChart>
					</ResponsiveContainer>
				</div>
			</Container>

			<Container>
				<div className={classnames(classes.tableWrapper, mixins.mb10)}>
					<div className={classes.tableTitle}>Продажи {brand.name} по категориям</div>
					<Divider style={{margin: "20px 0"}} />
					<Row>
						<Col xs={12} sm={12} md={12}>
							<Select size="large" value={state.categoriesTableDays} onChange={value => handlers.categoriesDaysOnChange(value, brandAlias)} dropdownMatchSelectWidth={false} style={{width: "100%"}}>
								<Option value={7}>За последние 7 дней</Option>
								<Option value={14}>За последние 14 дней</Option>
								<Option value={30}>За последние 30 дней</Option>
							</Select>
						</Col>
					</Row>
					<Divider style={{margin: "20px 0"}} />
					<Table
						columns={data.categoriesTableColumns}
						dataSource={state.categoriesTableData}
						expandable={{
							onExpand: (expanded, record) => {
								handlers.getCategoriesChildren({
									brandAlias: brandAlias,
									parentId: record.key,
									days: state.categoriesTableDays
								})
							},
						}}
						scroll={{x:"100%"}}
						loading={state.categoriesTableIsLoading}
						pagination={{
							pageSize: 5,
							size: state.isMobile ? "small" : "default",
						}}
						locale={{
							sortTitle: 'Сортировать',
							expand: 'Развернуть',
							collapse: 'Свернуть',
							triggerDesc: 'Нажмите для сортировки по убыванию',
							triggerAsc: 'Нажмите для сортировки по возрастанию',
							cancelSort: 'Нажмите для отмены сортировки',
						}}
					/>
				</div>
			</Container>

			<Container>
				<div className={classes.tableWrapper}>
					<div className={classes.tableTitle}>Продажи {brand.name} по товарам</div>
					<Divider style={{margin: "20px 0"}} />
					<Row>
						<Col xs={12} sm={12} md={12}>
							<Select size="large"  value={state.productsTableDays} onChange={value => handlers.productsTableDaysOnChange(value, brandAlias)} dropdownMatchSelectWidth={false} style={{width: "100%"}}>
								<Option value={7}>За последние 7 дней</Option>
								<Option value={14}>За последние 14 дней</Option>
								<Option value={30}>За последние 30 дней</Option>
							</Select>
						</Col>
					</Row>
					<Divider style={{margin: "20px 0"}} />
					<Table
						columns={data.productsTableColumns}
						dataSource={state.productsTableData}
						scroll={{x: "100%"}}
						loading={state.productsTableIsLoading}
						pagination={{
							onChange: (page, size) => {
								handlers.getProductsInfo({
									days: state.productsTableDays,
									page: page,
									alias: brandAlias,
									perPage: state.productsTablePerPage
								})
							},
							//showTotal: total => `Всего: ${total}`,
							total: state.productsTableTotal,
							current: state.productsTablePage,
							pageSize: state.productsTablePerPage,
							showSizeChanger: false,
							size: state.isMobile ? "small" : "default"
						}}	
						locale={{
							sortTitle: 'Сортировать',
							expand: 'Развернуть',
							collapse: 'Свернуть',
							triggerDesc: 'Нажмите для сортировки по убыванию',
							triggerAsc: 'Нажмите для сортировки по возрастанию',
							cancelSort: 'Нажмите для отмены сортировки',
						}}
					/>
				</div>
			</Container>

			<Container>
				<h1 className={classnames(classes.title, classes.titleFreeAnal)}>Бесплатная аналитика брендов Wildberries всегда под рукой</h1>
				<h5 className={classes.analSubheader}>Просто установите наш телеграм-бот и следите за любыми конкурентами!</h5>
			
				<Row className={classes.advantagesWrapper}>
					{data.advantages.map(item => (
						<Col key={item.title} className={classes.advantage}>
							<div className={classes.advantageIconWrapper}>
								<img alt="" src={item.icon} />
							</div>
							<div className={classes.advantageTitle}>{item.title}</div>
						</Col>
					))}
				</Row>
			</Container>
			
			<Container>
				<Row>
					<Col>
						<div className={classes.botWrapper}>
							<div className={classes.botLeft}>
								<img className={classes.iphone} alt="iphone screenshot" src={require("../../img/other/iphone.png")} />
								<img className={classes.iphoneShadow} alt="" src={require("../../img/other/shadow.png")} />
							</div>
							<div className={classes.botRight}>
								<div className={classes.title}>Следите за конкурентами в нашем telegram-боте</div>
								<div className={classes.description}>Наш онлайн-сервис синхронизируются с телеграм-ботом, телефоном и почтовым сервисом. Данные собираются ежедневно. Вы можете посмотреть статистику по любому бренду за любой период до 90 дней. Это удобно и бесплатно.</div>
								<button className={classes.tgButton}>
									<div>Установить telegram-бот</div>
									<img alt="" src={require("../../img/other/telegram.svg")} />
								</button>
								<img alt="" className={classes.arrow} src={require("../../img/other/arrow.svg")} />
							</div>
						</div>
					</Col>
				</Row>
			</Container>

			<Space variant="big" />
			
			<Container>
				<Row>
					<Col xs={12} sm={12} md={12} lg={8}>
						<div className={classes.howWeCountTitle}>Как рассчитываются продажи?</div>
						<div className={classes.howWeCountDescription}>
							Основная цель данной аналитики - показать распределение брендов по продажам на Wildberries. <br />
							Продажи пересчитывается ежедневно, и поставщик продукции может на него влиять.<br />
							<br />
							Например, следующим образом:<br />
							Периодически (не реже раз в 2 недели), выпускать новинки.<br />
							У бренда <b>{brand.name}</b> были дни, в которые не было новинок.<br />
							Не допускать того, чтобы закончился какой-то из размеров популярного товара.<br />
							У бренда <b>{brand.name}</b> было суммарно недоступно размеров: 325 шт.<br />
							Поддерживать остатки продаваемых товаров.<br />
							У бренда <b>{brand.name}</b> было суммарно недоступно товаров: 119 шт.<br />
							Не допускать того, чтобы месячная оборачиваемость артикула была более 60 дней, что будет провоцировать на дополнительных скидок и соответствующего уменьшения чистой прибыли.
						</div>
					</Col>
				</Row>
			</Container>
		</FragmentLifecycle>
		:
		<FragmentLifecycle
			didMount={() => {
				handlers.getExactBrandInfo({
					alias: brandAlias,
				})
			}}
			key="loading"
		>
			<div className={classes.loading}>
				<div className={classes.spinWrapper}>
					<Spin />
				</div>
			</div>
		</FragmentLifecycle>
	)
}

export default withRouter(Brand);