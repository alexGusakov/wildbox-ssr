import React from "react";
import { Container, Row, Col, Visible } from "react-grid-system";

import wildboxLogo from "../../img/other/wildbox-logo.svg";
import wildboxText from "../../img/other/wildbox-text.svg";

import classes from "./index.module.scss";

const Footer = props => (
	<Container className={classes.root}>
		<Row className={classes.row}>
			<Col xs={12} sm={12} md={12} lg={3} className={classes.c1}>
				<div className={classes.anyQuestions}>Остались вопросы?</div>
				<div className={classes.socialWrapper}>
					<a href="mailto:info@wildbox.ru" className={classes.link}>info@wildbox.ru</a>
					<div className={classes.divider} />
					<a href="#">
						<img alt="" className={classes.tgIcon} src={require("../../img/other/telegram.svg")} />
					</a>
				</div>
			</Col>

			<Visible lg xl xxl>
				<Col xs={12} sm={12} md={12} lg={4} className={classes.c2}>
					<div className={classes.logoWrapper}>
						<img alt="" className={classes.logo} src={wildboxLogo} />
						<img alt="" className={classes.text} src={wildboxText} />
					</div>
					<a href="#" className={classes.link}>Политика конфиденциальности</a>
				</Col>
			</Visible>

			<Col xs={12} sm={12} md={12} lg={5} className={classes.c3}>
				<div className={classes.subscribeInputWrapper}>
					<input type="text" placeholder="Email" className={classes.subscribeInput} />
					<button className={classes.subscribeButton}>Подписаться</button>
				</div>
				<div className={classes.subscribeDescription}>Делимся опытом, инсайдами и лайфхаками.</div>
			</Col>

			<Visible xs sm md>
				<Col xs={12} sm={12} md={12} lg={4} className={classes.c4}>
					<div className={classes.logoWrapper}>
						<img alt="" className={classes.logo} src={wildboxLogo} />
						<img alt="" className={classes.text} src={wildboxText} />
					</div>
					<a href="#" className={classes.link}>Политика конфиденциальности</a>
				</Col>
			</Visible>
		</Row>
	</Container>
)

export default Footer;