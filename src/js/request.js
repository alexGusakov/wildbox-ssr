import axios from "axios";

const Request = axios.create({
	baseURL: "https://wildbox.ru/api/",
	method: "post",
	timeout: 50000,
	//maxContentLength: 9999999
});

export default Request;