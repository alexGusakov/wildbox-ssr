import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import App from './components/App';
import ScrollToTop from 'react-router-scroll-top';
import * as serviceWorker from './serviceWorker';
import './css/global.scss';
import "./fonts/SanFrancisco/stylesheet.css";

ReactDOM.render(
	<React.StrictMode>
		<Router>
			<ScrollToTop>
				<App />
			</ScrollToTop>
		</Router>
	</React.StrictMode>,
	document.getElementById("root")
);

serviceWorker.unregister();